FROM mchoiruln/laravel-php:latest
#COPY composer.lock
COPY composer.json /var/www/
COPY database /var/www/database
WORKDIR /var/www
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('sha384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php --version=1.8.0 \
    && php -r "unlink('composer-setup.php');"
RUN php composer.phar install --no-dev --no-plugins --no-scripts \
    && rm composer.phar
COPY . /var/www
RUN chown -R www-data:www-data \
    /var/www/storage \
    /var/www/bootstrap/cache