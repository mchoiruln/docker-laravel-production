FROM php:7.3.1-fpm

RUN ulimit -n 2048 \
    && apt-get update \ 
    && apt-get install -y  \
        libzip-dev \
        unzip \
    && docker-php-ext-install -j$(nproc) zip